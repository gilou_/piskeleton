"""
This is a skeleton for everyday small python app

Argparse and config file are already here!
Datetime simple example are here as note.

"""
import os
import argparse
import config as cfg
from datetime import datetime, timedelta


"""
argparse validator for specifc datetime format
"""
def valid_date(s):
        try:
                return datetime.strptime(s, "%d/%m/%Y")
        except ValueError:
                msg = "Not a valid date: '{0}'.".format(s)
                raise argparse.ArgumentTypeError(msg)

"""
The main function
"""
def main():
        # create args parser object 
        parser = argparse.ArgumentParser(description = "CLI python core !") 
        
        # defining arguments for parser object 
        parser.add_argument("-v", "--verbose", help="increase output verbosity", action="store_true")
        parser.add_argument("int_arg", type=int, help="This line add an int argument")
        parser.add_argument("-s", "--startdate", help="The Start Date - format dd/mm/yyyy", required=True, type=valid_date)
        
        # parse the arguments from standard input 
        args = parser.parse_args()

        #get tasks list from cfg
        myTasks = cfg.tasks

        # How to use the verbose mode?!
        if args.verbose:
                print("Use verbose in developer's mode!")

        print("the start date is ", args.startdate.strftime("%d/%m/%Y"))

        # write the task list
        for task in myTasks:
                print(task[0], (args.startdate+timedelta(days=task[1])).strftime("%d/%m/%Y %M:%H"))


if __name__ == "__main__": 
        # calling the main function 
        main() 